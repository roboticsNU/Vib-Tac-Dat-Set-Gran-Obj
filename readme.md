Load reducedmodel.mat file in Matlab. 
It contains three variables: velocityData1, velocityData2, velocityData3.
And one more important variable: ans. Do not ever change it :fearful: !

Each of those three variables correspond to three different velocities. They share the same structure. Below the common structure is described. For simplicity, by *data* we denote one of those three variables (velocityDataX: with X=1,2 or 3).

## Structure of *data*:

```mermaid
graph TD;
element1["Data of Class 1"];
element2["Data of Class 2"];
element3["..."];
element4["Data of Class 15"];
data-->element1;
data-->element2;
data-->element3;
data-->element4;
```

As its seen from the diagram above the data is a matlab structure with 15 cells. Each cell correspond to the class label. 3 class data were completely removed from reducedmodel because they were not present in all velocityDataX variables. In order to access certain (let's say i'th where i is in range [1, 15]) class data simply type `velocityData1{i}`.

### The structure of i'th class data:
```mermaid
graph TD;
head["Data of Class i"];
element1["Class label"];
element2["Main data"];
head-->element1;
head-->element2;
```

Each class (there are 15 classes) has it's label, which is basically a name of an object that the robot arm was interacting with. 

### Main data structure
```mermaid
graph TD;
head["Main data"];
element1["pac0 data"];
element2["pac1 data"];
element3["pdc data"];
element4["electrodes data"];
element5["position states data"];
head-->element1;
head-->element2;
head-->element3;
head-->element4;
head-->element5; 
```

### pacs (0 and 1), pdc, electrodes and position states data structure
All the cells of the Main data in the diagram above share the common structure: they store five finger's data:

```mermaid
graph TD;
head["pac0, pac1, pdc, electrodes and position states data"];
element1["Finger 1 data"];
element2["Finger 2 data"];
element3["Finger 3 data"];
element4["Finger 4 data"];
element5["Finger 5 data"];
head-->element1;
head-->element2;
head-->element3;
head-->element4;
head-->element5;
```

Each finger data except for *electrodes data* and *position states data* is an array of numbers. There are 19 electrodes thus the finger data for *electrodes data* is also an array but each element of it is an array of size 19. Basically, if reshaped, the certain finger's electrodes data can be expressed as an array of size Nx19 where N is a number of data samples.

### The position states data of a certain finger
```mermaid
graph TD;
head["Position states data of a certain finger"];
element1["delta time"];
element2["Set Point Value"];
element3["Proc. Value"];
element4["Derivative of Proc. Value"];
element5["Error"];
element6["Coefficient of Prop."];
head-->element1;
head-->element2;
head-->element3;
head-->element4;
head-->element5;
head-->element6;
```

All of the cells except *Coefficient of Prop.* are arrays with the certain length and compared to pac0, pac1, pdc and electrodes data their length is 10 times less (due to different rate).

The *Coefficient of Prop.* is just a number, not an array. 

# Additional info
*command* data added in cmds.mat
